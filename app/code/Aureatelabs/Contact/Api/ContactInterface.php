<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Contact
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Contact\Api;

/**
 * Interface ContactInterface
 * @package Aureatelabs\Contact\Api
 */
interface ContactInterface
{

    /**
     * Returns greeting message to user
     *
     * @param \Aureatelabs\Contact\Api\Data\SubscriberInterface $subscriber
     * @return \Aureatelabs\Contact\Api\ResultInterface
     */
    public function post($subscriber);
}
